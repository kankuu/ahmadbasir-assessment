import { PartialType } from '@nestjs/mapped-types';
import { CreateDesaDto } from './create-desa.dto';

export class UpdateDesaDto extends PartialType(CreateDesaDto) {}
