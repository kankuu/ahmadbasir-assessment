import { Controller, Get, Post, Body, Patch, Param, Delete, Put, BadRequestException } from '@nestjs/common';
import { DesaService } from './desa.service';
import { CreateDesaDto } from './dto/create-desa.dto';
import { UpdateDesaDto } from './dto/update-desa.dto';

@Controller('svc')
export class DesaController {
  constructor(private readonly desaService: DesaService) {}

  @Post('simpan_desa/:token')
  async create(@Body() createDesaDto: CreateDesaDto, @Param() param:any) {
    if(!param.token){
      throw new BadRequestException("no token provided!")
    }
    return await this.desaService.create(createDesaDto);
  }

  @Get('get_desa/:token')
  async findAll(@Param() param:any) {
    return await this.desaService.findAll(param.token);
  }

  @Patch('update_desa/:token/:id')
  update(@Param('token') token:string,@Param('id') id: string, @Body() updateDesaDto: UpdateDesaDto) {
    if(!token){
      throw new BadRequestException("no token provided!")
    }
    return this.desaService.update(+id, updateDesaDto);
  }

  @Delete('destroy_desa/:id')
  remove(@Param('id') id: string) {
    return this.desaService.remove(+id);
  }
}
