import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateDesaDto } from './dto/create-desa.dto';
import { UpdateDesaDto } from './dto/update-desa.dto';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class DesaService {
  constructor(private prisma: PrismaService){}

  async create(createDesaDto: CreateDesaDto) {
    const kecamatan = await this.prisma.kecamatans.findUnique({
      where: {id: createDesaDto.kecamatanId}
    })
    if(kecamatan){
      return await this.prisma.desas.create({
        data: {
          namaDesa: createDesaDto.namaDesa,
          kecamatanId: createDesaDto.kecamatanId
        }
      })
    }else{
      throw new BadRequestException("kecamatan id not found!")
    }
  }

  async findAll(token:string) {
    if(!token){
      throw new BadRequestException("no token provided!")
    }
    return await this.prisma.desas.findMany()
  }

  findOne(id: number) {
    return `This action returns a #${id} desa`;
  }

  async update(id: number, updateDesaDto: UpdateDesaDto) {
    const desa = await this.prisma.desas.findUnique({
      where: {id: id}
    })

    if(desa) {
      return await this.prisma.desas.update({
        where: {
          id: id
        },
        data: {
          namaDesa: updateDesaDto.namaDesa
        }
      })
    }else {
      throw new BadRequestException("gagal melakukan update desa")
    }
  }

  async remove(id: number) {
  return await this.prisma.desas.delete({
    where: {id: id}
  })
  }
}
