import { Module } from '@nestjs/common';
import { DesaService } from './desa.service';
import { DesaController } from './desa.controller';
import { PrismaService } from 'src/prisma.service';

@Module({
  controllers: [DesaController],
  providers: [DesaService, PrismaService]
})
export class DesaModule {}
