import { Test, TestingModule } from '@nestjs/testing';
import { DesaController } from './desa.controller';
import { DesaService } from './desa.service';

describe('DesaController', () => {
  let controller: DesaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DesaController],
      providers: [DesaService],
    }).compile();

    controller = module.get<DesaController>(DesaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
