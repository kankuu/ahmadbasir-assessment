import { UserEntity } from "../entities/user.entity";

export class CreateUserDto {
  username: string
  password: string
  isAdmin: boolean | true
}
