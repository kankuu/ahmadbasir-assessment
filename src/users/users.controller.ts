import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { LoginUserDto } from './dto/login-user.dto';

@Controller('svc')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post('simpan_user')
  async create(@Body() createUserDto:CreateUserDto) {
    return await this.usersService.create(createUserDto);
  }

  @Post('getToken')
  async getToken(@Body() loginDto:LoginUserDto) {
    return await this.usersService.getToken(loginDto)
  }

  @Get('get_user/:token')
  async getUser(@Param() token: any) {
    return await this.usersService.getUser(token?.token)
  }
}
