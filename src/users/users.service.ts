import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { PrismaService } from '../prisma.service'

import * as bcrypt from 'bcrypt'
import { LoginUserDto } from './dto/login-user.dto';

import * as jwt from 'jsonwebtoken'

@Injectable()
export class UsersService {
  constructor(private prisma:PrismaService){}

  async create(userDto: CreateUserDto) {

    const user = await this.prisma.users.create({
      data: {
        username: userDto.username,
        password: await bcrypt.hash(userDto.password, 10),
        isAdmin: userDto.isAdmin
      }
    })

    return user;
  }

  async getToken(loginDto:LoginUserDto) {
    const user = await this.prisma.users.findFirst({
      where: {
        username: loginDto.username
      }
    })

    if(user){
      const validated = await bcrypt.compare(loginDto.password, user.password)
      delete user.password
      console.log("is validated: "+validated)
      if(validated){
        const token = jwt.sign(user, "1234567890", {
          expiresIn: 12345
        })
        return {
          token: token
        }
      }else {
        throw new UnauthorizedException("invalid username or password")
      }
    }else {
      throw new NotFoundException("user not found")
    }
  }

  async getUser(token:string) {
    const verify:any = await jwt.verify(token, "1234567890");
    const user = await this.prisma.users.findUnique({
      where: {id: verify?.id}
    })

    return user;
  }
}
