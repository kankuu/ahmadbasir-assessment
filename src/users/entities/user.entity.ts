import { Users } from "@prisma/client"
export class UserEntity implements Users{
  id: number;
  username: string;
  password: string;
  isAdmin: boolean;
}

