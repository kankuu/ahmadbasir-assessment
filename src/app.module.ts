import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { PrismaService } from './prisma.service';
import { KecamatanModule } from './kecamatan/kecamatan.module';
import { DesaModule } from './desa/desa.module';
import { ProvinsiModule } from './provinsi/provinsi.module';

@Module({
  imports: [UsersModule, KecamatanModule, DesaModule, ProvinsiModule],
  controllers: [AppController],
  providers: [AppService, PrismaService],
})
export class AppModule {}
