import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateKecamatanDto } from './dto/create-kecamatan.dto';
import { UpdateKecamatanDto } from './dto/update-kecamatan.dto';
import { PrismaService } from '../prisma.service'

@Injectable()
export class KecamatanService {
  constructor(private prisma: PrismaService){}

  async create(createKecamatanDto: CreateKecamatanDto) {
    return await this.prisma.kecamatans.create({
      data: {
        namaKecamatan: createKecamatanDto.namaKecamatan,
      }
    })
  }

  async findAll(token:string) {
    if(!token){
      throw new BadRequestException("no token provided!")
    }
    return await this.prisma.kecamatans.findMany()
  }
}
