import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { KecamatanService } from './kecamatan.service';
import { CreateKecamatanDto } from './dto/create-kecamatan.dto';
import { UpdateKecamatanDto } from './dto/update-kecamatan.dto';

@Controller('svc')
export class KecamatanController {
  constructor(private readonly kecamatanService: KecamatanService) {}

  @Post('simpan_kecamatan')
  async create(@Body() createKecamatanDto: CreateKecamatanDto) {
    return this.kecamatanService.create(createKecamatanDto);
  }

  @Get('get_kecamatan/:token')
  async findAll(@Param() param:any) {
    return this.kecamatanService.findAll(param.token);
  }
}
