import { Controller, Get, Post, Body, Patch, Param, Delete, BadRequestException } from '@nestjs/common';
import { ProvinsiService } from './provinsi.service';
import { CreateProvinsiDto } from './dto/create-provinsi.dto';
import { UpdateProvinsiDto } from './dto/update-provinsi.dto';

@Controller('svc')
export class ProvinsiController {
  constructor(private readonly provinsiService: ProvinsiService) {}

  @Post('simpan_provinsi')
  async create(@Body() createProvinsiDto: CreateProvinsiDto) {
    return await this.provinsiService.create(createProvinsiDto);
  }

  @Get('get_provinsi/:token')
  async findAll(@Param() param:any) {
    return await this.provinsiService.findAll(param.token);
  }
}
 