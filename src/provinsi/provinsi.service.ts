import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateProvinsiDto } from './dto/create-provinsi.dto';
import { UpdateProvinsiDto } from './dto/update-provinsi.dto';
import { PrismaService } from 'src/prisma.service';
import * as jwt from 'jsonwebtoken'
@Injectable()
export class ProvinsiService {

  constructor(private prisma: PrismaService){}

  async create(createProvinsiDto: CreateProvinsiDto) {
    return await this.prisma.provinsi.create({
      data: {
        namaProvinsi: createProvinsiDto.namaProvinsi
      }
    })
  }

  async findAll(token:string) {
    if(!token){
      throw new BadRequestException("no token provided!")
    }
    return await this.prisma.provinsi.findMany()
  }
}
